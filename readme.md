# Challenge API
This test was conducted on the basis of a spy agency exercise.[CHALLENGE](Programming_Challenge.pdf)
### Pre-requisitos 📋

```
Docker && Docker Compose
```
### Local 🔧
```
1 - cp api/.envfile.example api/.envfile

Copy the .envfile.example file and rename it to .envfile. Configure (api/.envfile) according to the environment variables you need.

Note: Database environment variables in docker-compose.yml file (docker) must be the same as the ones in your .envfile file.
```

```
2 - docker compose up -d
```

```
3 - docker exec -it django-app python manage.py loaddata roles permissions users
```

```
4 - Open in your favorite browser the following url [localhost:8080](http://localhost:8080/)
```
```
5 (Optional) - If you need to review the API documentation or test a request go to the following URL [playground](http://localhost:8000/graphql/)
```
## Users in database 
```
1) Super user
email: boss@gmail.com
password: development123
```
```
2) Manager, the emails addresses of 3 managers created are the same, only the number after the + sign must be change betweeen 1 and 3.

email: manager+1@gmail.com
password: development123
```

```
3) Hitman, the emails addresses of 9 hitmans created are the same, only the number after the + sign must be change betweeen 1 and 9.

email: hitman+1@gmail.com
password: development123
```

## Build with 🛠️
* [Docker](https://www.docker.com/)
* [Django 4.1](https://www.djangoproject.com/)
* [PostgreSQL 13](https://www.postgresql.org/)
* [GraphQL](https://graphql.org/learn/)
* [VueJs 2.0](https://vuejs.org/v2/guide/)
* [Apollo Vue](https://apollo.vuejs.org/)

