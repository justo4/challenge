export const errorsMixn = {
  methods: {
    /**
    * @author Jesus Miranda
    * function that formats errors
    *
    * @param {Object} errorsMain
    * @param {Object} errorsResponse
    */
    formatErrors (errorsMain, errorsResponse) {
      for (const key in errorsResponse) {
        if (errorsMain[key] !== undefined) {
          if (typeof errorsMain[key] !== 'object') {
            errorsMain[key] = errorsResponse[key][0].message
          } else {
            this.formatErrors(errorsMain[key], errorsResponse[key])
          }
        }
      }
    }
  }
}
