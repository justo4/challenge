export const ConvertMixn = {
  methods: {
    /**
      * @author Jesus Miranda
      * convert snake case to camel case
      *
      * @param {Object} ConvertMain
      * @param {Object} ConvertResponse
      */
    toCamel (string) {
      return string.replace(/([-_][a-z])/ig, ($1) => {
        return $1.toUpperCase()
          .replace('-', '')
          .replace('_', '')
      })
    }
  }
}
