export default {
  version: {
    local: {
      version: '1.0.0',
      branch: 'm1'
    },
    remote: { // Remote key setted automaticly
      version: '',
      branch: ''
    }
  },

  // Dashboard operations variables
  changelog: false,
  isCookies: false,
  isLoading: true,
  isMini: false,
  drawer: true,
  isDark: false,

  language: 'ES',

  width: {
    full: window.innerWidth,
    dialog: window.innerWidth * 0.8333333333 // 10 of 12 columns
  }
}
