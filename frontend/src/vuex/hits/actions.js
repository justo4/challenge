// Apollo
import apolloProvider from '@/plugins/apollo'
import list from 'GraphQL/queries/entities/hits/list.gql'

export default {
  async getList ({ state, dispatch, commit, rootState }, payload = false) {
    await apolloProvider.defaultClient.query({
      query: list,
      fetchPolicy: 'no-cache'
    }).then((response) => {
      const hitsList = response.data.hits.edges.map(item => {
        return {
          ...item.node
        }
      })
      commit('setList', hitsList)
    }).catch((errors) => {
      const errorObject = errors.message.replace(/'/g, '').split('code:')
      if (errorObject.length > 1 && errorObject[1] === '401') {
        dispatch('session/refreshToken', false, { root: true }).then(() => { dispatch('getList') })
      }
    }).finally(() => {})
  }
}
