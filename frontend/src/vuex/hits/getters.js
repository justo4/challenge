export default {
  defaultObject () {
    return {
      id: undefined,
      description: '',
      target: '',
      hitmenId: undefined
    }
  },
  defaultErrors () {
    return {
      description: '',
      target: '',
      hitmenId: ''
    }
  }
}
