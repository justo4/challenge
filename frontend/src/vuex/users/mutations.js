export default {
  setEntity (state, payload) {
    state.entity = payload
  },

  setList (state, payload) {
    state.list = payload
  }
}
