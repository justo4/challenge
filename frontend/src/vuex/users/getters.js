export default {
  defaultObject () {
    return {
      id: undefined,
      firstName: '',
      lastName: '',
      email: '',
      description: '',
      userHitmensIds: []
    }
  },
  defaultErrors () {
    return {
      firstName: '',
      lastName: '',
      email: '',
      description: '',
      userHitmensIds: ''
    }
  }
}
