
// Session module
import session from './session'

import users from './users'

import hits from './hits'

export default {
  session,
  users,
  hits
}
