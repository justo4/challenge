// Apollo
import apolloProvider from '@/plugins/apollo'
// Translation i18n
import { i18n } from '@/plugins'

export default {
  /**
  * @author Jesus Miranda
  * function to create, edit complete or delete
  * @param {Object} context {state, dispatch, commit, rootState}
  * @param {Object} payload {variables, mutation, method, name}
  **/
  graphqlBaseScaffold ({ state, dispatch, commit, rootState }, payload) {
    return new Promise((resolve, reject) => {
      apolloProvider.defaultClient.mutate({
        mutation: payload.mutation,
        variables: { input: payload.variables }
      }).then((response) => {
        const { errors, status, result } = response.data[payload.name]
        if (status === 'OK') {
          commit('toggleSnackbar', {
            message: i18n.t(`${payload.module}.helpers.success${payload.method.charAt(0).toUpperCase() + payload.method.slice(1)}`),
            color: 'green'
          }, { root: true })

          if (payload.method === 'delete') {
            resolve({ status: status })
          } else {
            resolve({ result: result, status: status })
          }
        } else if (status === 'NOT_FOUND') {
          commit('toggleSnackbar', {
            message: i18n.t(`errors.${status}`),
            color: 'warning'
          }, { root: true })
        } else if (status === 'UNAUTHORIZED') {
          commit('toggleSnackbar', {
            message: errors[0].message,
            color: 'warning'
          }, { root: true })
        } else {
          if (payload.method === 'delete') {
            commit('toggleSnackbar', undefined, { root: true })
          } else {
            reject(errors)
          }
        }
      }).catch((errors) => {
        const errorObject = errors.message.replace(/'/g, '').split('code:')
        if (errorObject.length > 1 && errorObject[1] === '401') {
          dispatch('session/refreshToken', false, { root: true }).then(() => { dispatch('removeProduct', payload) })
        } else {
          commit('toggleSnackbar', {
            message: errors.message.split(':')[1],
            color: 'warning'
          }, { root: true })
        }
      }).finally(() => {})
    })
  }
}
