export default {
  version (state) {
    return `${state.version.local.version}-${state.version.local.branch}`
  },

  isMobile (state) {
    return state.width.full <= 930
  }
}
