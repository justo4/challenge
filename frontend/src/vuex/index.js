import state from './state'
import mutations from './mutations'
import actions from './actions'
import getters from './getters'
import modules from './modules'
import plugins from './plugins'

export {
  state,
  mutations,
  actions,
  getters,
  plugins,
  modules
}
