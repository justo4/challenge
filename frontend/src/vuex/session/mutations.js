export default {
  setEntity (state, payload) {
    state.entity = payload
  },

  clearEntity (state, payload) {
    state.entity = {
      id: undefined,
      lastLogin: '',
      firstName: '',
      lastName: '',
      isActive: true,
      email: '',
      groups: []
    }
  }
}
