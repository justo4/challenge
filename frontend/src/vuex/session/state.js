export default {
  entity: {
    id: undefined,
    lastLogin: '',
    firstName: '',
    lastName: '',
    isActive: true,
    email: '',
    groups: [{ id: '' }]
  }
}
