// Libraries
import router from '@/plugins/router'
import cookies from 'js-cookie'
// Apollo
import apolloProvider from '@/plugins/apollo'

// graphql files
import login from '@/graphql/session/login.gql'
import refreshToken from '@/graphql/session/refreshToken.gql'
import me from '@/graphql/session/me.gql'

// Const
import { GC_AUTH_TOKEN, GC_REFRESH_TOKEN } from '@/plugins/const'

export default {
  async logout ({ dispatch, commit, state }) {
    cookies.remove(GC_AUTH_TOKEN)
    cookies.remove(GC_REFRESH_TOKEN)

    commit('clearEntity')
    router.push('/Login')
  },
  /**
  * @author Jesus Miranda
  * function refresh token
  *
  **/
  async refreshToken ({ dispatch, commit, rootState }) {
    await apolloProvider.defaultClient.mutate({
      mutation: refreshToken,
      variables: {
        refreshToken: cookies.get(GC_REFRESH_TOKEN)
      }
    }).then((result) => {
      const { token, errors, refreshToken } = result.data.refreshToken

      if (errors === null) {
        cookies.set(GC_AUTH_TOKEN, token)
        cookies.set(GC_REFRESH_TOKEN, refreshToken)
      } else {
        cookies.remove(GC_AUTH_TOKEN)
        cookies.remove(GC_REFRESH_TOKEN)
        router.push({ name: 'login' })
      }
    }).catch(() => {
      cookies.remove(GC_AUTH_TOKEN)
      cookies.remove(GC_REFRESH_TOKEN)
      router.push({ name: 'login' })
    })
  },
  /**
  * @author Jesus Miranda
  * function login
  *
  * @param {Object} payload {email, password}
  **/
  async login ({ state, dispatch, commit, rootState }, payload) {
    await apolloProvider.defaultClient.mutate({
      mutation: login,
      variables: {
        email: payload.email,
        password: payload.password
      }
    }).then((request) => {
      const { token, result, errors, refreshToken } = request.data.tokenCreate
      if (errors === null) {
        cookies.set(GC_AUTH_TOKEN, token)
        cookies.set(GC_REFRESH_TOKEN, refreshToken)
        commit('setEntity', result)
        dispatch('loadData')
      } else {
        errors.forEach(key => {
          commit('toggleSnackbar', {
            message: key.message,
            color: 'orange'
          }, { root: true })
        })
      }
    }).catch(() => {
      commit('toggleSnackbar', undefined, { root: true })
    })
  },
  /**
  * @author Jesus Miranda
  * function load entity information
  *
  **/
  async loadEntity ({ state, dispatch, commit, rootState }) {
    await apolloProvider.defaultClient.query({
      query: me,
      fetchPolicy: 'no-cache'
    }).then((result) => {
      const user = result.data.me
      commit('setEntity', user)
    }).catch(() => {
      commit('toggleSnackbar', undefined, { root: true })
    })
  },
  /**
  * @author Jesus Miranda
  * function load data
  *
  * @param {Object} payload {}
  **/
  async loadData ({ dispatch, commit, getters, state }, payload) {
    const lastUrl = window.location.href.split('#')[1]
    const token = cookies.get(GC_AUTH_TOKEN)
    if (token !== '') {
      await dispatch('loadEntity')
    }
    await dispatch('users/getList', false, { root: true })
    await dispatch('hits/getList', false, { root: true })

    let redirect = lastUrl

    if (lastUrl === '/Login') {
      redirect = '/Hits'
    }
    await router.push({ path: redirect }).catch(() => {})
  }
}
