export default {
  avatar (state) {
    return `${state.entity.firstName[0]}${state.entity.lastName[0]}`
  },

  fullName (state) {
    return `${state.entity.firstName} ${state.entity.lastName}`
  },

  groups (state) {
    return state.entity.groups
  }
}
