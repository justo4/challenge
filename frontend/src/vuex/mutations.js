import cookies from 'js-cookie'

export default {
  setChangelog (state, payload) {
    state.changelog = payload
  },

  setWidth (state, payload) {
    if (payload < 930) {
      state.width = {
        full: payload,
        dialog: payload
      }
    } else {
      state.width = {
        full: payload,
        dialog: payload * 0.8333333333
      }
    }
  },

  toggleDark (state) {
    state.isDark = !state.isDark
    cookies.set('dark', state.isDark)
  },

  setDark (state, payload) {
    state.isDark = payload
    cookies.set('dark', state.isDark)
  },

  toggleDrawer (state) {
    state.drawer = !state.drawer
  },

  setDrawer (state, payload) {
    state.drawer = payload
  },

  setCookies (state, payload) {
    cookies.set('state', payload)
    state.isCookies = payload
  },
  toggleSnackbar (state, payload) {}
}
