import Login from 'pages/Login'
import Users from 'pages/Users'
import Hits from 'pages/Hits'
import Register from 'pages/Register'

import Layout from '@/layouts/Main'
import LayoutLogin from '@/layouts/Login'

import NotFound from 'pages/NotFound'

export default [
  {
    path: '/',
    redirect: '/Hits'
  },
  {
    path: '/',
    component: Layout,
    children: [
      {
        path: 'Users',
        name: 'users',
        components: { default: Users }
      },
      {
        path: 'Hits',
        name: 'hits',
        components: { default: Hits }
      }
    ]
  },
  {
    path: '/',
    component: LayoutLogin,
    children: [
      {
        path: 'Login',
        name: 'login',
        components: { default: Login }
      },
      {
        path: 'Register',
        name: 'register',
        components: { default: Register }
      }
    ]
  },
  {
    path: '*',
    component: NotFound
  }

]
