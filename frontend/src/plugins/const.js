export const GC_AUTH_TOKEN = 'graph-jwt-auth-token'
export const GC_REFRESH_TOKEN = 'graph-jwt-refresh-token'

export const MENU = [
  {
    title: 'Hits',
    icon: 'mdi-clipboard-list-outline',
    to: '/Hits',
    level: 3
  },
  {
    title: 'Users',
    icon: 'mdi-account-details',
    to: '/Users',
    level: 2
  }
]
