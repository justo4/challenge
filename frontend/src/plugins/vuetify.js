
import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

import es from 'vuetify/es5/locale/es'

const colors = {
  primary: '#077570',
  secondary: '#ffffff'
}
// Create component
Vue.use(Vuetify)

export default new Vuetify({
  lang: {
    locales: { es },
    current: 'es'
  },

  icons: {
    iconfont: 'mdi',
    values: {
      cancel: 'mdi-close',
      prev: 'mdi-chevron-left',
      next: 'mdi-chevron-right'
    }
  },

  theme: {
    dark: false,
    themes: {
      dark: colors,
      light: colors
    },

    options: {
      customProperties: true
    }
  }
})
