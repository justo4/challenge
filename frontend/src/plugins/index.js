import vuetify from './vuetify'
import apolloProvider from './apollo'
import router from './router'
import store from './store'
import cookies from './cookies'
import i18n from './i18n'

export { vuetify, i18n, router, store, cookies, apolloProvider }
