import Cookies from 'js-cookie'

const cookies = {}

cookies.install = (Vue) => {
  Vue.prototype.$cookies = Cookies
}

export default cookies
