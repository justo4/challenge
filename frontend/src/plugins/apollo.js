// Main imports
import Vue from 'vue'
import VueApollo from 'vue-apollo'
import cookies from 'js-cookie'

// Apollo client dependencies
import { ApolloClient } from 'apollo-client'
/* import { HttpLink } from 'apollo-link-http' */
import { InMemoryCache } from 'apollo-cache-inmemory'
import { ApolloLink } from 'apollo-link'
import { createUploadLink } from 'apollo-upload-client'

import { GC_AUTH_TOKEN } from './const'

const httpLink = createUploadLink({
  uri: (process.env.NODE_ENV === 'development' ? `http://${window.location.hostname}:8000/api/` : `${window.location.protocol}//${window.location.hostname}/api/`)
})

/* const httpLink = new HttpLink({
  uri: (process.env.NODE_ENV === 'development' ? 'http://localhost:8000/api/' : `${window.location.protocol}//${window.location.hostname}/api/`)
}) */
const cache = new InMemoryCache()

const authMiddleware = new ApolloLink((operation, forward) => {
  const token = cookies.get(GC_AUTH_TOKEN)
  operation.setContext({
    headers: {
      authorization: token ? `JWT ${token}` : null
    }
  })
  return forward(operation)
})

const apolloClient = new ApolloClient({
  link: authMiddleware.concat(httpLink),
  cache: cache,
  connectToDevTools: true
})

Vue.use(VueApollo)

const apolloProvider = new VueApollo({
  defaultClient: apolloClient,
  defaultOptions: {
    $loadingKey: 'loading'
  }
})

export default apolloProvider
