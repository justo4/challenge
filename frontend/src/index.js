import Vue from 'vue'
import App from './App'
import '@mdi/font/css/materialdesignicons.css'
import cookiesLib from 'js-cookie'

import { vuetify, router, i18n, store, cookies, apolloProvider } from '@/plugins'
import { GC_AUTH_TOKEN } from '@/plugins/const'

import { sync } from 'vuex-router-sync'

const unsync = sync(store, router) /* eslint-disable-line no-unused-vars */

router.beforeEach((to, from, next) => {
  const token = cookiesLib.get(GC_AUTH_TOKEN)
  const group = store.getters['session/groups']
  if (group.length > 0) {
    if (parseInt(group[0].id) === 3 && to.name === 'users') {
      next({ path: '/Unauthorized' })
    }
  }
  if (to.name !== 'login' && to.name !== 'register' && !token) {
    next({ path: '/Login' })
  }
  next()
})

Vue.use(
  cookies
)

new Vue({
  router,
  vuetify,
  i18n,
  store,
  apolloProvider,
  computed: {
    isAuthenticated () {
      return this.$cookies.get(GC_AUTH_TOKEN)
    }
  },
  created () {
    if (this.isAuthenticated) {
      this.$store.dispatch('session/loadData')
    }
    this.$vuetify.theme.dark = this.$store.state.isDark
  },
  render: h => h(App)
}).$mount('#app')
