import { render } from '@testing-library/vue'
import Home from '@/pages/Home'

describe('Home.vue', () => {
  it('renders props.msg when passed', () => {
    const name = 'tester'

    const { findByText } = render(Home, {
      propsData: { name }
    })

    findByText('Hello tester from my Vue.js page, built with Webpack 4!')
  })
})
