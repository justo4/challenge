from django.middleware.csrf import _mask_cipher_secret, _get_new_csrf_string

def _get_new_csrf_token():
    return _mask_cipher_secret(_get_new_csrf_string())