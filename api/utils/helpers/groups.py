from abc import ABC, abstractmethod

# Models
from hits.models import Hit
from entities.models import User
from django.db.models.query import QuerySet


class FactoryGroup(ABC):
    """
    factorial base class in charge of listing the different types of elements according to their roles
    """

    @abstractmethod
    def list_hits(self, user: User):
        pass

    @abstractmethod
    def list_hitmens(self, user: User):
        pass


class Administrator(FactoryGroup):
    """
    Administrator class in charge of listing the hits and hitmen that can see this role
    """
    def list_hits(self, user: User) -> QuerySet:
        """ List of hits by role
        
        Parameters:
            user: Model user

        Returns:
            Queryset: list of hits
        """
        return Hit.objects.filter(is_deleted=False).order_by('id')

    def list_hitmens(self, user: User) -> QuerySet:
        """ List of hitmens by role
        
        Parameters:
            user: Model user

        Returns:
            Queryset: list of hitmens
        """
        return User.objects.all()


class General(FactoryGroup):
    """
    General class in charge of listing the hits and hitmen that can see this role
    """
    def list_hits(self, user: User) -> QuerySet:
        """ List of hits by role
        
        Parameters:
            user: Model user

        Returns:
            Queryset: list of hits
        """
        return Hit.objects.filter(
            hitmen__id__in=[user.id, *user.user_hitmens.values_list('id', flat=True)]
        ).order_by('id')

    def list_hitmens(self, user: User) -> QuerySet:
        """ List of hitmens by role
        
        Parameters:
            user: Model user

        Returns:
            Queryset: list of hitmens
        """
        return User.objects.filter(
            id__in=user.user_hitmens.values_list('id', flat=True)
        )


class Hitmen(FactoryGroup):
    """
    Hitmen class in charge of listing the hits and hitmen that can see this role
    """
    def list_hits(self, user: User) -> QuerySet:
        """ List of hits by role
        
        Parameters:
            user: Model user

        Returns:
            Queryset: list of hits
        """
        return Hit.objects.filter(
            hitmen_id=user.id,
            is_deleted=False
        ).order_by('id')

    def list_hitmens(self, user: User) -> QuerySet:
        """ List of hitmens by role
        
        Parameters:
            user: Model user

        Returns:
            Queryset: list of hitmens
        """
        return User.objects.filter(id=user.id)


class ClassBuilder:
    """class in charge of returning the class according to the sent role"""
    classes = {
        1: Administrator(),
        2: General(),
        3: Hitmen(),
    }

    def get_class_for_listing(self, group_id: int) -> FactoryGroup:
        """returns the class according to the role
        
        Parameters:
            group_id: Group id

        Returns:
            FactoryGroup: class by role
        """
        try:
            return self.classes[group_id]
        except KeyError:
            raise KeyError
