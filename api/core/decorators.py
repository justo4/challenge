from graphql_core.core.exceptions import LoginRequired
from django.utils.functional import wraps
from graphql.execution.base import ResolveInfo
from graphql_core.core.exceptions import PermissionDenied, LoginRequired
from enum import Enum
from typing import Iterable, Union


def context(f):
    def decorator(func):
        def wrapper(*args, **kwargs):
            info = next(arg for arg in args if isinstance(arg, ResolveInfo))
            return func(info.context, *args, **kwargs)

        return wrapper

    return decorator

def account_passes_test(test_func):
    """Determine if user has permission to access to content."""

    def decorator(f):
        @wraps(f)
        @context(f)
        def wrapper(context, *args, **kwargs):
            test_func(context)
            return f(*args, **kwargs)

        return wrapper

    return decorator

def permission_required(perm: Union[Enum, Iterable[Enum]]):
    def check_perms(context):
        if isinstance(perm, Enum):
            perms = (perm,)
        else:
            perms = perm

        requestor = context.user
        if not requestor.has_perms(perms):
            raise PermissionDenied(permissions=perms)

    return account_passes_test(check_perms)

def login_required():
    def check_perms(context):
        requestor = context.user
        if requestor.is_anonymous:
                raise LoginRequired()
            
    return account_passes_test(check_perms)