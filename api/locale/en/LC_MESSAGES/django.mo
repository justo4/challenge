��    *      l  ;   �      �     �  (   �  j   �     O     e     w     �  
   �  .   �     �  "   �  
          3   2     f     t     �     �     �  '   �     �     �  �        �       1   "     T  9   f     �  1   �  :   �          -     <     O     _     m  	   �  ,   �     �  
   �  B  �     
  (   '
  j   P
     �
     �
     �
     �
  
     .        H  "   Y  
   |     �  3   �     �     �     �     �       '         H     Q  �   q     c     x  1   �     �  9   �       1     :   M     �     �     �     �     �     �  	   �  ,   �     %  
   3                    #          &           !                      
         "                   )   *                           '                         (                              $      	         %       Account inactive. Account needs to be confirmed via email. CSRF token required to refresh token. This argument is required when refreshToken is provided as a cookie. Can't find id: %(id)s Create JWT token. Create a new hit. Error decoding signature First name Forbidden response, means 403 HTTP status code ID of the hitman ID of the role to which it belongs ID of user Incorrect refreshToken Internal Error response, means 500 HTTP status code Invalid token Login required Manage staff. Manage users. Missing refreshToken OK response, means 200 HTTP status code Password Please, enter valid credentials Refresh JWT token. Mutation tries to take refreshToken from the input.If it fails it will try to take refreshToken from the http-only cookie -%(JWT_REFRESH_TOKEN_COOKIE_NAME)s. csrfToken is required when refreshToken is provided as a cookie. Register a new user. Signature has expired Unauthorized response, means 403 HTTP status code Unknown HTTP CODE Unprocessable Entity response, means 422 HTTP status code Update a user. You do not have permission to perform this action You need one of the following permissions: %(permissions)s confirm password delete a user. description of hit duplicate email email address invalid phone number last name password and confirm_password does not match target of hit user email Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Account inactive. Account needs to be confirmed via email. CSRF token required to refresh token. This argument is required when refreshToken is provided as a cookie. Can't find id: %(id)s Create JWT token. Create a new hit. Error decoding signature First name Forbidden response, means 403 HTTP status code ID of the hitman ID of the role to which it belongs ID of user Incorrect refreshToken Internal Error response, means 500 HTTP status code Invalid token Login required Manage staff. Manage users. Missing refreshToken OK response, means 200 HTTP status code Password Please, enter valid credentials Refresh JWT token. Mutation tries to take refreshToken from the input.If it fails it will try to take refreshToken from the http-only cookie -%(JWT_REFRESH_TOKEN_COOKIE_NAME)s. csrfToken is required when refreshToken is provided as a cookie. Register a new user. Signature has expired Unauthorized response, means 403 HTTP status code Unknown HTTP CODE Unprocessable Entity response, means 422 HTTP status code Update a user. You do not have permission to perform this action You need one of the following permissions: %(permissions)s confirm password delete a user. description of hit duplicate email email address invalid phone number last name password and confirm_password does not match target of hit user email 