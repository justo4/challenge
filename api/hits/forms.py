"""Forms file"""
# Django
from django import forms

# Models
from entities.models import User


class HitForm(forms.Form):
    """Hit form"""
    id = forms.IntegerField(
        required=False,
        min_value=1
    )

    target = forms.CharField(
        required=True,
        max_length=60,
        min_length=3
    )

    hitmen_id = forms.ModelChoiceField(
        queryset=None
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        users_inactives = User.objects.filter(
            is_active=False,
            groups__in=[2, 3]
        ).exclude(pk=self.data.hitmen_id)

        self.fields['hitmen_id'].queryset = User.objects.exclude(
            pk__in=users_inactives
        )
