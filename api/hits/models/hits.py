# Django
from django.db import models
from core.models import CommonInfo


class Hit(CommonInfo):

    hitmen = models.ForeignKey(
        "entities.User",
        on_delete=models.CASCADE,
        related_name="hit_hitmen",
        null=False,
        default=2
    )

    description = models.CharField(max_length=255, null=True)

    target = models.CharField(max_length=60, null=True)

    status = models.CharField(
        null=False,
        default="ASSIGNED",
        max_length=50,
        choices=[
            ("ASSIGNED", "assigned"),
            ("FAILED", "failed"),
            ("COMPLETED", "completed"),
        ]
    )

    created_by = models.ForeignKey(
        "entities.User",
        on_delete=models.CASCADE,
        related_name="hit_created_by",
        null=False
    )

    def __str__(self):
        return "hit for {target}".format(target=self.target)
