"""Production settings."""

from .base import *  # NOQA
import os

DEBUG = False
ENVIRONMENT = os.getenv('ENVIRONMENT', 'production')

# Base
SECRET_KEY = os.getenv('DJANGO_SECRET_KEY')
ALLOWED_HOSTS = os.getenv('DJANGO_ALLOWED_HOSTS').split(',')
