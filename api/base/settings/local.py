"""Development settings."""

import os
from .base import *  # NOQA


# Base
DEBUG = True

# Security
ALLOWED_HOSTS = [
    "localhost",
    "0.0.0.0",
    "127.0.0.1",
]

# Cache
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': ''
    }
}

# Email
EMAIL_BACKEND = os.getenv('DJANGO_EMAIL_BACKEND', default='django.core.mail.backends.console.EmailBackend')
EMAIL_HOST = 'localhost'
EMAIL_PORT = 1025

# LOGGING = {
#     'version': 1,
#     'disable_existing_loggers': False,
#     'formatters': {
#         'simple': {
#             'format': '[{levelname}] [{asctime} @ {name}] => {message}',
#             'style': '{',
#         },
#     },

#     'handlers': {
#         'console': {
#         'level': 'DEBUG',
#         'class': 'logging.StreamHandler',
#         'formatter': 'simple',
#         },
#         'db_file': {
#             'level': 'DEBUG',
#             'class': 'logging.FileHandler',
#             'filename': BASE_DIR+'/logs/db.log',
#             'formatter': 'simple',
#         },
#     },
#     'loggers': {
#         'django.request': {
#             'handlers': ['console'],
#             'level': 'DEBUG',
#             'propagate': True
#         },
#         'django.db.backends': {
#             'handlers': ['db_file'],
#             'level': 'DEBUG',
#             'propagate': True
#         },
#     }
# }