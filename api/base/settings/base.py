# System
import os
import warnings
from django.core.management.utils import get_random_secret_key

# Utils
from pytimeparse import parse
from dotenv import load_dotenv
from datetime import timedelta

# sentry
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

# support for the old version of django while accepting PR in graphene-django <= 2.15.0
import django
from django.utils.encoding import force_str
django.utils.encoding.force_text = force_str

BASE_DIR = os.path.dirname(os.path.dirname(
    os.path.dirname(os.path.abspath(__file__))))

env_path = os.path.join(BASE_DIR, '.envfile')

load_dotenv(dotenv_path=env_path, verbose=True)

# Base
DEBUG = os.getenv('DEBUG')

# Users & Authentication
AUTH_USER_MODEL = 'entities.User'

# URL
URL_BASE = os.getenv('URL_BASE', default='http://localhost:8000')

# Secret key
SECRET_KEY = os.getenv('SECRET_KEY')

if not SECRET_KEY and DEBUG:
    warnings.warn("SECRET_KEY not configured, using a random temporary key.")
    SECRET_KEY = get_random_secret_key()

# Urls
ROOT_URLCONF = 'base.urls'

# Apps
DJANGO_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.staticfiles',
]

THIRD_PARTY_APPS = [
    'graphene_django',
    'django_filters',
    'phonenumber_field',
    'graphql_playground',
    'naomi',
]

LOCAL_APPS = [
    'core',
    'entities',
    'graphql_core',
    'utils',
    'hits',
]

INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

AUTHENTICATION_BACKENDS = [
    "core.auth.backend.JSONWebTokenBackend"
]
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    # 'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'core.middleware.authenticate',
    'core.middleware.jwt_refresh_token_middleware',
]
if DEBUG:
    # app
    INSTALLED_APPS += ['corsheaders']

    # middleware
    MIDDLEWARE += ['corsheaders.middleware.CorsMiddleware']

    # allow all
    CORS_ORIGIN_ALLOW_ALL = True

    # methods allow
    CORS_ALLOW_METHODS = [
        'POST',
        'GET'
    ]

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'templates')
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'base.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': os.getenv('DATABASE_ENGINE', 'django.db.backends.postgresql_psycopg2'),
        'NAME': os.getenv('DATABASE_NAME', 'base_development'),
        'USER': os.getenv('DATABASE_USERNAME', 'postgres'),
        'PASSWORD': os.getenv('DATABASE_PASSWORD', ''),
        'HOST': os.getenv('DATABASE_HOST', '127.0.0.1'),
        'PORT': os.getenv('DATABASE_PORT', '5432')
    }
}

AUTH_PASSWORD_VALIDATORS = [
    {'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator', },
    {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator', },
    {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator', },
    {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator', },
]

LANGUAGE_CODE = 'es'

LANGUAGES = (
    ('en', ('English')),
    ('es', ('Spanish')),
)
LOCALE_PATHS = [os.path.join(BASE_DIR, 'locale')]
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True


# Static files
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

# Media
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

# Files
FILES_URL = '/files/'
FILE_ROOT = os.path.join(BASE_DIR, 'files')

# Graphene
GRAPHENE = {
    'SCHEMA': 'base.schema.schema',  # this file doesn't exist yet
}

RSA_PRIVATE_KEY = os.environ.get("RSA_PRIVATE_KEY", None)
RSA_PRIVATE_PASSWORD = os.environ.get("RSA_PRIVATE_PASSWORD", None)
JWT_MANAGER_PATH = os.environ.get(
    "JWT_MANAGER_PATH", "core.jwt.manager.JWTManager"
)

JWT_EXPIRE = os.environ.get("JWT_EXPIRE", True)

JWT_TTL_ACCESS = timedelta(seconds=parse(os.environ.get("JWT_TTL_ACCESS", "5 minutes")))

JWT_TTL_APP_ACCESS = timedelta(
    seconds=parse(os.environ.get("JWT_TTL_APP_ACCESS", "1 hour"))
)
JWT_TTL_REFRESH = timedelta(seconds=parse(os.environ.get("JWT_TTL_REFRESH", "30 days")))

JWT_TTL_REQUEST_EMAIL_CHANGE = timedelta(
    seconds=parse(os.environ.get("JWT_TTL_REQUEST_EMAIL_CHANGE", "1 hour")),
)

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

#EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

EMAIL_USE_TLS = True
EMAIL_USE_SSL = False
EMAIL_HOST = 'smtp.gmail.com'

EMAIL_HOST_USER = os.getenv('EMAIL_HOST_USER', 'no-reply@example.com')
EMAIL_HOST_PASSWORD = os.getenv('EMAIL_HOST_PASSWORD', '')

EMAIL_PORT = 587

# Naomi
if DEBUG:
    EMAIL_BACKEND = "naomi.mail.backends.naomi.NaomiBackend"
    EMAIL_FILE_PATH = os.path.join(BASE_DIR, 'tmp/')

# Security
SESSION_COOKIE_HTTPONLY = True
CSRF_COOKIE_HTTPONLY = True
SECURE_BROWSER_XSS_FILTER = True
X_FRAME_OPTIONS = 'DENY'

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

ENVIRONMENT = os.getenv('ENVIRONMENT', 'development')
# Sentry configurations 
if not DEBUG:
    SENTRY_DNS = os.getenv(
        'SENTRY_DNS', 
        'https://8dbc0af8ef254e6fb4cb79866bdc6122@o1345619.ingest.sentry.io/6622428'
    )
    sentry_sdk.utils.MAX_STRING_LENGTH = 4096
    sentry_sdk.init(
        dsn=SENTRY_DNS,
        integrations=[
            DjangoIntegration(),
        ],
        traces_sample_rate=1.0,
        send_default_pii=True,
        environment=ENVIRONMENT
    )