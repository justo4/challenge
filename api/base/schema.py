import graphene

from graphql_core.entities.schema import EntitiesMutations, EntitiesQueries
from graphql_core.hits.schema import HitsMutations, HitsQueries


class Query(EntitiesQueries, HitsQueries, graphene.ObjectType):
    pass


class Mutation(EntitiesMutations, HitsMutations, graphene.ObjectType):
    pass


schema = graphene.Schema(query=Query, mutation=Mutation)
