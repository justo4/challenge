# Django
from base.settings.base import URL_BASE
from django.urls import path
from django.views.decorators.csrf import csrf_exempt

# Graphql & Graphene
from graphene_django.views import GraphQLView
from graphql_playground.views import GraphQLPlaygroundView


urlpatterns = [
    path("api/", csrf_exempt(GraphQLView.as_view(graphiql=True))),
    path(
        "graphql/",
        csrf_exempt(
            GraphQLPlaygroundView.as_view(endpoint=URL_BASE + '/api/')
        )
    ),
]
