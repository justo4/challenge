# Queries

# Permissions
from core.permissions import AccountPermissions, get_permissions
from core.decorators import permission_required, login_required


@login_required()
@permission_required(AccountPermissions.MANAGE_STAFF)
def resolve_permissions(info):
    return get_permissions()
