# Queries

# Models
from django.contrib.auth.models import Group

# permissions
from core.permissions import AccountPermissions
from core.decorators import permission_required, login_required

@login_required()
@permission_required(AccountPermissions.MANAGE_STAFF)
def resolve_groups(info):
    return Group.objects.all()
