# Utils
from core.permissions import AccountPermissions
from core.decorators import permission_required, login_required
from utils.helpers.groups import ClassBuilder


@login_required()
@permission_required(AccountPermissions.VIEW_USERS)
def resolve_users(info):
    group_id = info.context.user.groups.first().id

    try:
        hits_helper = ClassBuilder().get_class_for_listing(group_id)
        return hits_helper.list_hitmens(info.context.user)
    except KeyError:
        return []


@login_required()
def resolve_me(info):
    return info.context.user
