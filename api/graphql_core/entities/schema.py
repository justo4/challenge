
# Graphql & Graphene
from graphene import ObjectType, Field

# Mutations
from graphql_core.entities.mutations.users import (
    CreateUser,
    EditUser,
    DeleteUser
)

from graphql_core.entities.mutations.authenticate import (
    CreateToken,
    RefreshToken,
)
# Queries
from graphql_core.entities.queries.permissions import resolve_permissions
from graphql_core.entities.queries.groups import resolve_groups
from graphql_core.entities.queries.users import resolve_users, resolve_me

# Types
from graphql_core.entities.types.permissions import PermissionsType
from graphql_core.entities.types.groups import GroupsType
from graphql_core.entities.types.users import UserType
from graphql_core.core.types.base import CustomConnectionField

# Filters
from graphql_core.entities.filters.groups import GroupsFilter
from graphql_core.entities.filters.users import UsersFilter
from graphql_core.entities.filters.permissions import PermissionsFilter


class EntitiesMutations(ObjectType):

    token_create = CreateToken.Field()
    token_refresh = RefreshToken.Field()

    user_create = CreateUser.Field()
    user_edit = EditUser.Field()
    user_delete = DeleteUser.Field()


class EntitiesQueries(ObjectType):

    permissions = CustomConnectionField(
        PermissionsType, filterset_class=PermissionsFilter)

    groups = CustomConnectionField(GroupsType, filterset_class=GroupsFilter)

    users = CustomConnectionField(UserType, filterset_class=UsersFilter)
    me = Field(UserType)

    def resolve_permissions(root, info, **kwargs):
        return resolve_permissions(info)

    def resolve_groups(root, info, **kwargs):
        return resolve_groups(info)

    def resolve_users(root, info, **kwargs):
        return resolve_users(info)

    def resolve_me(root, info, **kwargs):
        return resolve_me(info)
