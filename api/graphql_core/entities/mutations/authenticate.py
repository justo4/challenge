# Base
from typing import Optional
from entities.helpers.error_codes import AccountErrorCode
from graphql_core.core.mutations import BaseMutation

# Graphql & Graphene
from graphene import Field, String

# models
from entities.models import User as User

# Types
from graphql_core.entities.types.users import UserType

# security and exceptions
import jwt
from utils.csrf import _get_new_csrf_token
from core.permissions import AccountPermissions
from django.core.exceptions import ValidationError

# const
from core.jwt.core import (
    JWT_REFRESH_TOKEN_COOKIE_NAME,
    JWT_REFRESH_TYPE,
    create_access_token,
    create_refresh_token,
    get_user_from_payload,
    jwt_decode
)

# Debugs and utils
from django.utils import timezone
from django.utils.translation import gettext as _
from django.utils.crypto import get_random_string


def get_payload(token):
    try:
        payload = jwt_decode(token)
    except jwt.ExpiredSignatureError:
        raise ValidationError(
            _("Signature has expired"), 
            code=AccountErrorCode.JWT_SIGNATURE_EXPIRED
        )
    except jwt.DecodeError:
        raise ValidationError(
            _("Error decoding signature"), 
            code=AccountErrorCode.JWT_DECODE_ERROR
        )
    except jwt.InvalidTokenError:
        raise ValidationError(
            _("Invalid token"), 
            code=AccountErrorCode.JWT_INVALID_TOKEN
        )
    return payload


def get_user(payload):
    try:
        user = get_user_from_payload(payload)
    except Exception:
        user = None
    if not user:
        raise ValidationError(
            _("Invalid token"), 
            code=AccountErrorCode.JWT_INVALID_TOKEN
        )
    return user


class CreateToken(BaseMutation):
    
    token = String()
    refresh_token = String()
    csrf_token = String()
    result = Field(UserType)
    form = None

    class Meta:
        description = _("Create JWT token.")

    class Arguments:
        email = String(required=True)
        password = String(required=True)

    @classmethod
    def _retrieve_user_from_credentials(cls, username: str, password: str) -> Optional[User]:
        user = User.objects.filter(**{User.USERNAME_FIELD: username}).first()
        if user and user.check_password(password):
            return user
        return None

    @classmethod
    def get_user(cls, _info, data: dict):
        user = cls._retrieve_user_from_credentials(data['email'], data['password'])
        if not user:
            raise ValidationError({
                'email': ValidationError(
                    _("Please, enter valid credentials"),
                    code=AccountErrorCode.INVALID_CREDENTIALS
                )
            })
        if not user.is_active:
            raise ValidationError({
                'email': ValidationError(
                    _("Account inactive."),
                    code=AccountErrorCode.INACTIVE
                )
            })

        return user
    @classmethod
    def perform_mutation(cls, self, info, **kwargs):
        user = cls.get_user(info, kwargs)
        access_token = create_access_token(user)
        csrf_token = _get_new_csrf_token()
        refresh_token = create_refresh_token(user, {'csrfToken': csrf_token})
        info.context.refresh_token = refresh_token
        info.context._cached_user = user
        user.last_login = timezone.now()
        user.save(update_fields=['last_login', 'updated_at'])
        return cls(
            result=user,
            token=access_token,
            refresh_token=refresh_token,
            csrf_token=csrf_token,
        )


class RefreshToken(BaseMutation):
    """Mutation that refresh user token and returns token and user data."""

    token = String()

    result = Field(UserType)

    
    class Arguments:
        refresh_token = String(required=False, description="Refresh token.")
        csrf_token = String(
            required=False,
            description=_(
                "CSRF token required to refresh token. This argument is "
                "required when refreshToken is provided as a cookie."
            ),
        )

    class Meta:
        description = _(
            "Refresh JWT token. Mutation tries to take refreshToken from the input."
            "If it fails it will try to take refreshToken from the http-only cookie -"
            "%(JWT_REFRESH_TOKEN_COOKIE_NAME)s. csrfToken is required when refreshToken "
            "is provided as a cookie."
        ) % {'JWT_REFRESH_TOKEN_COOKIE_NAME': JWT_REFRESH_TOKEN_COOKIE_NAME}

    @classmethod
    def get_refresh_token_payload(cls, refresh_token):
        try:
            payload = get_payload(refresh_token)
        except ValidationError as e:
            raise ValidationError({'refreshToken': e})
        return payload

    @classmethod
    def get_refresh_token(cls, info, data):
        request = info.context
        refresh_token = request.COOKIES.get(JWT_REFRESH_TOKEN_COOKIE_NAME, None)
        refresh_token = data.get('refresh_token') or refresh_token
        return refresh_token

    @classmethod
    def clean_refresh_token(cls, refresh_token):
        if not refresh_token:
            raise ValidationError({
                'refresh_token': ValidationError(
                    _("Missing refreshToken"),
                    code=AccountErrorCode.JWT_MISSING_TOKEN
                )
            })
        payload = cls.get_refresh_token_payload(refresh_token)
        if payload['type'] != JWT_REFRESH_TYPE:
            raise ValidationError({
                'refresh_token': ValidationError(
                    _("Incorrect refreshToken"),
                    code=AccountErrorCode.JWT_INVALID_TOKEN
                )
            })
        return payload
    
    @classmethod
    def get_user(cls, payload):
        try:
            user = get_user(payload)
        except ValidationError as e:
            raise ValidationError({'refresh_token': e})
        return user
    
    @classmethod
    def perform_mutation(cls, root, info, **data):
        refresh_token = cls.get_refresh_token(info, data)
        payload = cls.clean_refresh_token(refresh_token)

        user = cls.get_user(payload)
        token = create_access_token(user)

        return cls(result=user, token=token)
