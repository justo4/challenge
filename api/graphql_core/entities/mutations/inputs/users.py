"""Inputs"""
# Graphene
from graphene import InputObjectType, String, ID, List
from django.utils.translation import gettext as _


class UserBaseInput(InputObjectType):
    """
    Users!
    """

    password1 = String(
        description=_("Password")
    )

    password2 = String(
        description=_("confirm password")
    )

    email = String(
        description=_("user email")
    )


class UserCreateInput(UserBaseInput):
    first_name = String(
        description=_("First name")
    )
    
    last_name = String(
        description=_("last name")
    )


class UserEditInput(UserBaseInput):

    id = ID(
        description=_("ID of user")
    )
    
    first_name = String(
        description=_("First name")
    )
    
    description = String(
        description=_("Description of user")
    )

    last_name = String(
        description=_("last name")
    )

    group_ids = List(ID,
        description=_("ID of the role to which it belongs")
    )
    hitmen_ids = List(ID,
        description=_("ID of the hitmen")
    )


class UserDeleteInput(InputObjectType):

    id = ID(
        description=_("ID of user")
    )