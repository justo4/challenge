# Mutations

# class Base
from graphql_core.core.mutations import BaseMutation

# Graphql & Graphene
from graphene import Field

# Models
from entities.models import User
from django.contrib.auth.models import Group

# Forms
from entities.forms import UserCreateForm, UserEditForm
from django.forms import ValidationError

# Types
from graphql_core.entities.types.users import UserType

# Utils
from datetime import datetime
from django.utils.translation import gettext as _
from core.permissions import AccountPermissions
from entities.helpers.error_codes import AccountErrorCode
from graphql_core.entities.mutations.inputs.users import (
    UserCreateInput,
    UserEditInput,
    UserDeleteInput
)


class CreateUser(BaseMutation):

    result = Field(UserType)
    form = UserCreateForm

    class Arguments:
        input = UserCreateInput()

    class Meta:
        description = _("Register a new user.")
        permissions = ()

    @classmethod
    def perform_mutation(cls, self, info, **kwargs):
        validator = cls.form(kwargs['input'])
        if validator.is_valid():
            instance = cls.construct_instance(User(), kwargs['input'])
            instance.set_password(kwargs['input'].get('password1'))
            cls.save(info, instance)
            
            instance.groups.add(*Group.objects.filter(pk=3))

            return cls(
                status=200,
                result=instance
            )
        else:
            raise ValidationError(validator.errors)


class EditUser(BaseMutation):
    result = Field(UserType)
    form = UserEditForm

    class Arguments:
        input = UserEditInput()
    
    class Meta:
        description = _("Update a user.")
        permissions = (AccountPermissions.EDIT_USERS,)

    @classmethod
    def perform_mutation(cls, self, info, **kwargs):
        validator = cls.form(kwargs['input'])

        if validator.is_valid():
            try:
                entity = User.objects.get(
                    pk=kwargs['input']['id'], is_active=True)
            except Exception as e:
                raise ValidationError({
                    'id': ValidationError(
                        message=_("Can't find id: %(id)s")\
                        % {'id': kwargs['input'].get('id')},
                        code=AccountErrorCode.NOT_FOUND
                    )
                })

            instance = cls.construct_instance(entity, kwargs['input'])
            cls.save(info, instance)

            if info.context.user.groups.first().id == 1:
                instance.user_hitmens.clear()

                instance.user_hitmens.add(
                    *User.objects.filter(pk__in=kwargs['input'].get("hitmen_ids", [])))

            if 'group_ids' in kwargs['input']:
                instance.groups.clear()
                instance.groups.add(*Group.objects.filter(pk__in=kwargs['input']['group_ids']))

            return cls(
                status=200,
                result=instance
            )
        else:
            raise ValidationError(validator.errors)

class DeleteUser(BaseMutation):

    class Arguments:
        input = UserDeleteInput()
    
    class Meta:
        description = _("delete a user.")
        permissions = (AccountPermissions.DELETE_USERS,)

    @classmethod
    def perform_mutation(cls, self, info, **kwargs):
        try:
            entity = User.objects.get(
                pk=kwargs['input']['id'], is_active=True)
        except Exception as e:
            raise ValidationError({
                'id': ValidationError(
                    message=_("Can't find id: %(id)s")\
                    % {'id': kwargs['input'].get('id')},
                    code=AccountErrorCode.NOT_FOUND
                )
            })
        entity.is_active = False
        entity.is_deleted = True
        entity.deleted_at = datetime.now()
        entity.save()
        return cls(
            status=200,
        )
