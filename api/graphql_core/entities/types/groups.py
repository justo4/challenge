# Types

# Models
from django.contrib.auth.models import Group

# Graphql & Graphene
from graphene import List
from graphql_core.core.types.base import BaseType

# Types
from .permissions import PermissionsType


class GroupsType(BaseType):
    class Meta:
        model = Group

    permissions = List(PermissionsType)

    def resolve_permissions(self, info):
        return self.permissions.all()
