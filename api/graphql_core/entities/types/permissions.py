# Types

# Graphql & Graphene
from graphql_core.core.types.base import BaseType

# Models
from django.contrib.auth.models import Permission


class PermissionsType(BaseType):
    class Meta:
        model = Permission
