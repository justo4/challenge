# Types

# Models
from django.contrib.auth import get_user_model

# Graphql & Graphene
from graphql_core.core.types.base import BaseType
from graphene import ID, List


class UserType(BaseType):
    class Meta:
        model = get_user_model()
        exclude = ('is_superuser', 'is_staff')

    group_id = ID()

    def resolve_group_id(self, info):
        try:
            return self.groups.first().id
        except Exception as e:
            return None

    user_hitmens_ids = List(ID)

    def resolve_user_hitmens_ids(self, info):
        try:
            return self.user_hitmens.all().values_list("id", flat=True)
        except Exception as e:
            return []
