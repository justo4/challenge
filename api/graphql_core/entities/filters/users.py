# Types

# Filter & Ordering
from django_filters import FilterSet, rest_framework as filters

# Models
from django.contrib.auth.models import User


class UsersFilter(FilterSet):

    id = filters.NumberFilter(label='id', required=False)

    class Meta:
        model = User
        fields = ('id', 'email')
