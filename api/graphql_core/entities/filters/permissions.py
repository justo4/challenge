# Types

# Filter & Ordering
from django_filters import FilterSet

# Models
from django.contrib.auth.models import Permission


class PermissionsFilter(FilterSet):
    class Meta:
        model = Permission
        fields = ('codename', )
