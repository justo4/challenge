# Types

# Filter & Ordering
from django_filters import FilterSet

# Models
from django.contrib.auth.models import Group


class GroupsFilter(FilterSet):
    class Meta:
        model = Group
        fields = ('id',)
