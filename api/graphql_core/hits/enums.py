from graphene import Enum


class HitStatusEnum(Enum):
    ASSIGNED = 'ASSIGNED'
    FAILED = 'FAILED'
    COMPLETED = 'COMPLETED'
