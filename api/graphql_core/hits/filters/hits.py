# Types

# Filter & Ordering
from django_filters import FilterSet

# Models
from hits.models import Hit


class HitsFilter(FilterSet):
    class Meta:
        model = Hit
        fields = ('id',)
