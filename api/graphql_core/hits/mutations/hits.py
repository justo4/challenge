# Mutations

# class Base
from graphql_core.core.mutations import BaseMutation

# Graphql & Graphene
from graphene import Field, List

# Types
from graphql_core.hits.types.hits import HitsType

# Models
from hits.models import Hit
from entities.models import User

# Forms
from hits.forms import HitForm

# Input
from graphql_core.hits.mutations.inputs import (
    HitCreateInput,
    HitEditInput,
    HitEditBulkInput,
    HitDeleteInput
)

# Utils
from graphql_core.hits.enums import HitStatusEnum
from django.utils.translation import gettext as _
from django.forms import ValidationError
from core.permissions import HitPermissions
from entities.helpers.error_codes import AccountErrorCode


class CreateHit(BaseMutation):

    result = Field(HitsType)
    form = HitForm

    class Arguments:
        input = HitCreateInput()

    class Meta:
        description = _("Create a new hit.")
        permissions = (HitPermissions.CREATE_HITS,)

    @classmethod
    def perform_mutation(cls, self, info, **kwargs):
        validator = cls.form(kwargs['input'])
        if validator.is_valid():
            kwargs['input']['status'] = HitStatusEnum.ASSIGNED.value

            instance = cls.construct_instance(Hit(), kwargs['input'])
            instance.hitmen_id = kwargs['input'].get('hitmen_id')
            instance.created_by_id = info.context.user.id
            cls.save(info, instance)

            return cls(
                status=200,
                result=instance
            )
        else:
            raise ValidationError(validator.errors)


class EditHit(BaseMutation):

    result = Field(HitsType)
    form = HitForm

    # Input
    class Arguments:
        input = HitEditInput()

    class Meta:
        description = _("Update a hit.")
        permissions = (HitPermissions.EDIT_HITS,)

    @classmethod
    def perform_mutation(cls, self, info, **kwargs):
        validator = cls.form(kwargs['input'])

        if validator.is_valid():
            try:
                entity = Hit.objects.get(
                    pk=kwargs['input']['id'],
                    status=HitStatusEnum.ASSIGNED.value,
                    is_deleted=False
                )
            except Exception as e:
                raise ValidationError({
                    'id': ValidationError(
                        message=_("Can't find id: %(id)s")
                        % {'id': kwargs['input'].get('id')},
                        code=AccountErrorCode.NOT_FOUND
                    )
                })
            instance = cls.construct_instance(entity, kwargs['input'])

            if info.context.user.groups.first().id == 3:
                instance.hitmen_id = entity.hitmen_id
            else:
                instance.hitmen_id = kwargs['input'].get('hitmen_id')

            cls.save(info, instance)

            return cls(status=200, result=instance)

        else:
            raise ValidationError(validator.errors)


class EditBulkHit(BaseMutation):

    result = List(HitsType)
    form = HitForm

    # Input
    class Arguments:
        input = List(HitEditBulkInput)

    class Meta:
        description = _("Bulk update of many hits.")
        permissions = (HitPermissions.EDIT_HITS,)

    @classmethod
    def perform_mutation(cls, self, info, **kwargs):
        entities = []
        user_hitmen = User.objects.filter(
            id__in=info.context.user.user_hitmens.values_list(
                'id', flat=True
            )
        ).values_list('id', flat=True)
        for hit in kwargs['input']:
            try:
                entity = Hit.objects.get(
                    pk=hit['id'],
                    status=HitStatusEnum.ASSIGNED.value,
                    is_deleted=False
                )

                if int(hit['hitmen_id']) in user_hitmen:
                    entity.hitmen_id = hit['hitmen_id']
                    entities.append(entity)
            except Exception as e:
                pass

        Hit.objects.bulk_update(entities, ['hitmen_id'])
        return cls(status=200, result=entities)


class DeleteHit(BaseMutation):

    class Arguments:
        input = HitDeleteInput()

    class Meta:
        description = _("Delete a hit.")
        permissions = (HitPermissions.DELETE_HITS,)

    @classmethod
    def perform_mutation(cls, self, info, **kwargs):
        try:
            entity = Hit.objects.get(
                pk=kwargs['input']['id'],
                status=HitStatusEnum.ASSIGNED.value,
                is_deleted=False
            )
        except Exception as e:
            raise ValidationError({
                'id': ValidationError(
                    message=_("Can't find id: %(id)s")
                    % {'id': kwargs['input'].get('id')},
                    code=AccountErrorCode.NOT_FOUND
                )
            })
        entity.is_deleted = True
        entity.deleted_at = datetime.now()
        return cls(
            status=200,
        )
