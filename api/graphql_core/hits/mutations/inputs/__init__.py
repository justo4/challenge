from .hits import (
    HitCreateInput, 
    HitEditInput, 
    HitEditBulkInput, 
    HitDeleteInput
)