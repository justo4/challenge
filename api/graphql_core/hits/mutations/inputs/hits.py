"""Inputs"""
# Graphene
from graphene import InputObjectType, String, ID

# enums
from django.utils.translation import gettext as _
from graphql_core.hits.enums import HitStatusEnum


class HitBaseInput(InputObjectType):
    """ base class of the hits """
    target = String(
        description=_("target of hit")
    )

    description = String(
        description=_("description of hit")
    )

    hitmen_id = ID(
        description=_("ID of the hitman")
    )


class HitCreateInput(HitBaseInput):
    """ input type for the creation of hit """
    pass


class HitEditInput(HitBaseInput):
    """ input type for the update of hit """
    id = ID(
        description=_("ID of hit")
    )

    status = HitStatusEnum()


class HitEditBulkInput(InputObjectType):
    """ input type for the bulk update of many hits """
    id = ID(
        description=_("ID of hit")
    )

    hitmen_id = ID(
        description=_("ID of the hitman")
    )


class HitDeleteInput(InputObjectType):
    """ input type for the delete of hit """

    id = ID(
        description=_("ID of hit")
    )
