# permissions
from core.permissions import HitPermissions
from core.decorators import permission_required, login_required
from utils.helpers.groups import ClassBuilder


@login_required()
@permission_required(HitPermissions.VIEW_HITS)
def resolve_hits(info):
    group_id = info.context.user.groups.first().id
    try:
        hits_helper = ClassBuilder().get_class_for_listing(group_id)
        return hits_helper.list_hits(info.context.user)
    except KeyError:
        return []
