# Models
from hits.models import Hit

# Graphql & Graphene
from graphene import ID, String
from graphql_core.core.types.base import BaseType


class HitsType(BaseType):
    class Meta:
        model = Hit

    created_by_id = ID()

    def resolve_created_by_id(self, info):
        return self.created_by.id

    hitmen_id = ID()

    def resolve_hitmen_id(self, info):
        return self.hitmen.id

    hitmen_email = String()

    def resolve_hitmen_email(self, info):
        return self.hitmen.email
