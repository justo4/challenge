
# Graphql & Graphene
from graphene import ObjectType

# Mutations
from graphql_core.hits.mutations.hits import (
    CreateHit,
    EditHit,
    DeleteHit,
    EditBulkHit,
)

# Queries
from graphql_core.hits.queries.hits import resolve_hits

# Types
from graphql_core.hits.types.hits import HitsType
from graphql_core.core.types.base import CustomConnectionField

# Filters
from graphql_core.hits.filters.hits import HitsFilter


class HitsMutations(ObjectType):

    hit_create = CreateHit.Field()
    hit_edit = EditHit.Field()
    hit_edit_bulk = EditBulkHit.Field()
    hit_delete = DeleteHit.Field()


class HitsQueries(ObjectType):

    hits = CustomConnectionField(HitsType, filterset_class=HitsFilter)

    def resolve_hits(root, info, **kwargs):
        return resolve_hits(info)
