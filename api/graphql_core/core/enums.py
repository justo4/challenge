"""Enum main classes"""
from graphene import Enum
from django.utils.translation import gettext as _


class Statuses(Enum):
    """Statuses enum"""
    OK = 200
    NOT_FOUND = 404
    INTERNALERROR = 500
    UNPROCESSABLE = 422
    UNAUTHORIZED = 401
    FORBIDDEN = 403
    BAD_REQUEST = 400

    @property
    def description(self):
        """Description property"""

        description = ""
        if self == Statuses.OK:
            description = _("OK response, means 200 HTTP status code")
        elif self == Statuses.INTERNALERROR:
            description = _("Internal Error response, means 500 HTTP status code")
        elif self == Statuses.UNPROCESSABLE:
            description = _("Unprocessable Entity response, means 422 HTTP status code")
        elif self == Statuses.UNAUTHORIZED:
            description = _("Unauthorized response, means 403 HTTP status code")
        elif self == Statuses.FORBIDDEN:
            description = _("Forbidden response, means 403 HTTP status code")
        elif self == Statuses.BAD_REQUEST:
            description = _("Bad Request response, means 400 HTTP status code")
        else:
            description = _("Unknown HTTP CODE")

        return description