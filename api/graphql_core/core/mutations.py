"""Base mutations"""

# Graphql & Graphene
from typing import Tuple
from graphene import (
    Mutation, 
    Field, 
    List, 
    ObjectType, 
    String
)
from graphene.types.mutation import MutationOptions
from graphql_core.core.exceptions import PermissionDenied

# Enums
from django.core.exceptions import ValidationError
from graphql_core.core.enums import Statuses
from utils.error_codes import get_error_code_from_error

# Model Types
from django.db.models import  AutoField, FileField


class ErrorType(ObjectType):
    field = String()
    message = String()
    code = String()


class BaseMutation(Mutation):
    status = Field(Statuses)
    errors = List(ErrorType)
    
    class Meta:
        abstract = True

    @classmethod
    def __init_subclass_with_meta__(
        cls,
        description=None,
        permissions: Tuple = None,
        _meta=None,
        **options,
    ):
        if not _meta:
            _meta = MutationOptions(cls)
        _meta.permissions = permissions
        super().__init_subclass_with_meta__(
            description=description, _meta=_meta, **options
        )

    @classmethod
    def perform_mutation(cls, root, info, **data):
        pass
    
    @classmethod
    def handle_errors(cls, error: ValidationError):
        errors_list=[]
        if hasattr(error, 'error_dict'):
            # convert field errors
            for field, field_errors in error.error_dict.items():
                for err in field_errors:
                    errors_list.append({
                        'field': field,
                        'message': err.messages[0],
                        'code': get_error_code_from_error(err)
                    })
        else:
            # convert non-field errors
            for err in error.error_list:
                errors_list.append({
                    'message': err.messages[0],
                    'code': get_error_code_from_error(err)
                })
        return errors_list

    @classmethod
    def construct_instance(cls, instance, data):
        """Fill instance fields with data.

        The `instance` argument is either an empty instance of a already
        existing one which was fetched from the database. `data` is
        data to be set in instance fields. Returns `instance` with filled
        fields, but not saved to the database.
        """

        for f in instance._meta.fields:

            if any(
                [
                    not f.editable,
                    isinstance(f, AutoField),
                    f.name not in data,
                ]
            ):
                continue
            value = data[f.name]
            if value is None:
                # We want to reset the file field value when None was passed
                # in the input, but `FileField.save_form_data` ignores None
                # values. In that case we manually pass False which clears
                # the file.
                if isinstance(f, FileField):
                    value = False
                if not f.null:
                    value = f._get_default()
            f.save_form_data(instance, value)
        return instance

    @classmethod
    def clean_instance(cls, info, instance):
        """Clean the instance that was created using the input data.

        Once an instance is created, this method runs `full_clean()` to perform
        model validation.
        """
        try:
            instance.full_clean()
        except ValidationError as error:
            if hasattr(cls._meta, "exclude"):
                # Ignore validation errors for fields that are specified as
                # excluded.
                new_error_dict = {}
                for field, errors in error.error_dict.items():
                    if field not in cls._meta.exclude:
                        new_error_dict[field] = errors
                error.error_dict = new_error_dict

            if error.error_dict:
                raise error

    @classmethod
    def save(cls, info, instance):
        instance.save()

    @classmethod
    def check_permissions(cls, context, permissions=None):
        """Determine whether user has rights to perform this mutation.

        Default implementation assumes that account is allowed to perform any
        mutation. By overriding this method or defining required permissions
        in the meta-class, you can restrict access to it.

        The `context` parameter is the Context instance associated with the request.
        """
        permissions = permissions or cls._meta.permissions
        if not permissions:
            return True
        if context.user.is_authenticated:
            if context.user.has_perms(permissions):
                return True
        return False
    
    @classmethod
    def mutate(cls, root, info, **data):
        try:
            if not cls.check_permissions(info.context):
                permissionsException = PermissionDenied()
                return cls(errors=[{
                    'message': str(permissionsException),
                    'code': permissionsException.code
                }], status=401)

            response = cls.perform_mutation(root, info, **data)
            return response
        except ValidationError as e:
            return cls(errors=cls.handle_errors(e), status=400)
        