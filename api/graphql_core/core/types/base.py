"""Base Types"""

# Graphql & Graphene
import graphene
from graphene import Field, Int, List
from graphene_django import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField


class BaseType(DjangoObjectType):

    pk = Field(type=Int, source='id')

    class Meta:
        abstract = True


class CustomConnectionField(DjangoFilterConnectionField):
    def __init__(self, type, fields=None, order_by=None,
                 extra_filter_meta=None, filterset_class=None,
                 *args, **kwargs):
        self._type = type
        self._fields = fields
        self._provided_filterset_class = filterset_class
        self._filtering_args = None
        self._filterset_class = None
        self._extra_filter_meta = extra_filter_meta
        self._base_args = None
        super(DjangoFilterConnectionField, self).__init__(
            type,
            *args,
            **kwargs
        )

    @property
    def type(self):
        node_type = super(graphene.relay.ConnectionField, self).type

        class Connection(graphene.Connection):
            total_count = graphene.Int()

            class Meta:
                name = node_type._meta.name + 'Connection'
                node = node_type

            def resolve_total_count(self, info, **kwargs):
                return self.iterable.count()

        return Connection
