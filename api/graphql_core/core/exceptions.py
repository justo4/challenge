from django.utils.translation import gettext_lazy as _
from enum import Enum
from typing import Sequence

class PermissionDenied(Exception):
    def __init__(self, message=None, *, permissions: Sequence[Enum] = None):
        if not message:
            if permissions:
                permission_list = ", ".join(p.name for p in permissions)
                message = (
                    _("You need one of the following permissions: %(permissions)s")\
                    % {'permissions': permission_list}
                )
            else:
                message = _("You do not have permission to perform this action")
        super().__init__(message)
        self.permissions = permissions
        self.code = "permission_denied"


class LoginRequired(Exception):
    def __init__(self, message=None, *, permissions: Sequence[Enum] = None):
        if not message:
            message = _("Login required")
        super().__init__(message)
        self.permissions = permissions
        self.code = "login_required"

