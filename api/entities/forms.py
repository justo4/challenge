"""Forms file"""
# Django
from django import forms

# Phone number
from phonenumber_field.formfields import PhoneNumberField

# Models
from .models import User

# helpers
from django.utils.translation import gettext as _

class UserBaseForm(forms.Form):

    id = forms.IntegerField(
        required=False,
        min_value=1
    )

    phone_number = PhoneNumberField(
        error_messages={
            'invalid': _("invalid phone number"),
        },
        required=False
    )

    first_name = forms.CharField(
        max_length=120,
        min_length=2,
        required=False
    )
    
    last_name = forms.CharField(
        max_length=120,
        min_length=2,
        required=False
    )

    password1 = forms.CharField(
        max_length=120,
        min_length=8,
        required=False
    )

    email = forms.EmailField(required=True)

    def clean_email(self):
        flag = User.objects.filter(email__iexact=self.cleaned_data.get('email'))

        if len(flag) > 0:
            if int(flag[0].id) != self.cleaned_data.get('id'):
                raise forms.ValidationError(
                    _("duplicate email"), 
                    code="invalid"
                )

    def clean_password1(self):
        password = self.data.get('password1', '')
        confirm_password = self.data.get('password2', '')

        if password != confirm_password:
            raise forms.ValidationError(
                _("password and confirm_password does not match")
            )


class UserCreateForm(UserBaseForm):
    """User form"""

    password1 = forms.CharField(
        max_length=120,
        min_length=8,
        required=True
    )

    password2 = forms.CharField(
        max_length=120,
        min_length=8,
        required=True
    )

    email = forms.EmailField(required=True)


class UserEditForm(UserBaseForm):
    """User form"""

    id = forms.IntegerField(
        required=True,
        min_value=1
    )

    email = forms.EmailField(required=True)


class GroupForm(forms.Form):
    """Group form"""
    id = forms.IntegerField(
        required=False,
        min_value=1
    )

    name = forms.CharField(
        max_length=120,
        min_length=2,
        required=True
    )
