# Django
from typing import Union
from django.db import models
from django.contrib.auth.models import (
    Group, 
    _user_has_perm, 
    Permission,
    PermissionsMixin, 
    AbstractBaseUser, 
)
from django.utils.translation import gettext as _
from django.utils.crypto import get_random_string
from django.db.models import Q, QuerySet
from django.db.models.expressions import Exists, OuterRef

# Phone number
from phonenumber_field.modelfields import PhoneNumberField

# utils
from django.utils import timezone

# Permissions
from core.permissions import AccountPermissions, BasePermissionEnum, get_permissions

def get_jwt_token_key_random():
    return get_random_string(12)

class User(PermissionsMixin, AbstractBaseUser):

    email = models.EmailField(
        unique=True,
        blank=False, 
        max_length=254, 
        verbose_name=_("email address")
    )

    first_name = models.CharField(max_length=256, blank=True)
    last_name = models.CharField(max_length=256, blank=True)
    
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    
    date_joined = models.DateTimeField(default=timezone.now, editable=False)
    
    phone_number = PhoneNumberField(null=False)
    description = models.CharField(max_length=255, null=True)
    hitmens = models.ManyToManyField('entities.User', related_name='user_hitmens')

    is_deleted = models.BooleanField(default=False)
    deleted_at = models.DateTimeField(null=True)
    updated_at = models.DateTimeField(auto_now=True)
    jwt_token_key = models.CharField(max_length=12, default=get_jwt_token_key_random)

    USERNAME_FIELD = "email"

    class Meta:
        ordering = ("email",)
        permissions = (
            (AccountPermissions.MANAGE_USERS.codename, _("Manage users.")),
            (AccountPermissions.MANAGE_STAFF.codename, _("Manage staff.")),
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._effective_permissions = None

    @property
    def effective_permissions(self) -> "QuerySet[Permission]":
        if self._effective_permissions is None:
            self._effective_permissions = get_permissions()
            if not self.is_superuser:

                UserPermission = User.user_permissions.through
                user_permission_queryset = UserPermission.objects.filter(
                    user_id=self.pk
                ).values("permission_id")

                UserGroup = User.groups.through
                GroupPermission = Group.permissions.through
                user_group_queryset = UserGroup.objects.filter(user_id=self.pk).values(
                    "group_id"
                )
                group_permission_queryset = GroupPermission.objects.filter(
                    Exists(user_group_queryset.filter(group_id=OuterRef("group_id")))
                ).values("permission_id")

                self._effective_permissions = self._effective_permissions.filter(
                    Q(
                        Exists(
                            user_permission_queryset.filter(
                                permission_id=OuterRef("pk")
                            )
                        )
                    )
                    | Q(
                        Exists(
                            group_permission_queryset.filter(
                                permission_id=OuterRef("pk")
                            )
                        )
                    )
                )
        return self._effective_permissions

    @effective_permissions.setter
    def effective_permissions(self, value: "QuerySet[Permission]"):
        self._effective_permissions = value
        # Drop cache for authentication backend
        self._effective_permissions_cache = None

    def has_perm(self, perm: Union[BasePermissionEnum, str], obj=None):  # type: ignore
        # This method is overridden to accept perm as BasePermissionEnum
        perm = perm.value if hasattr(perm, "value") else perm  # type: ignore

        # Active superusers have all permissions.
        if self.is_active and self.is_superuser and not self._effective_permissions:
            return True
        return _user_has_perm(self, perm, obj)
