from enum import Enum


class AccountErrorCode(Enum):
    GRAPHQL_ERROR = "graphql_error"
    INACTIVE = "inactive"
    INVALID = "invalid"
    INVALID_PASSWORD = "invalid_password"
    INVALID_CREDENTIALS = "invalid_credentials"
    NOT_FOUND = "not_found"
    PASSWORD_ENTIRELY_NUMERIC = "password_entirely_numeric"
    PASSWORD_TOO_COMMON = "password_too_common"
    PASSWORD_TOO_SHORT = "password_too_short"
    PASSWORD_TOO_SIMILAR = "password_too_similar"
    REQUIRED = "required"
    UNIQUE = "unique"
    JWT_SIGNATURE_EXPIRED = "signature_has_expired"
    JWT_INVALID_TOKEN = "invalid_token"
    JWT_DECODE_ERROR = "decode_error"
    JWT_MISSING_TOKEN = "missing_token"
    JWT_INVALID_CSRF_TOKEN = "invalid_csrf_token"
    ACCOUNT_NOT_CONFIRMED = "account_not_confirmed"

